//
//  main.m
//  AESTest
//
//  Created by zmjios on 2016/10/25.
//  Copyright © 2016年 zmjios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#include <stdlib.h>



int main(int argc, char * argv[]) {
    
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
