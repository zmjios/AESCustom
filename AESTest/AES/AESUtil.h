//
//  AESUtil.h
//  AESTest
//
//  Created by zmjios on 2016/10/26.
//  Copyright © 2016年 zmjios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AESUtil : NSObject


+ (NSString *)encryptAES:(NSString *)content key:(NSString *)key ;

+ (NSString *)decryptAES:(NSString *)content key:(NSString *)key;

@end
