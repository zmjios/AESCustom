//
//  NSData+AES.h
//  AESTest
//
//  Created by zmjios on 2016/10/26.
//  Copyright © 2016年 zmjios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES)


- (NSData *)AES256EncryptWithKey:(NSString *)key;


- (NSData *)AES256DecryptWithKey:(NSString *)key;

@end
