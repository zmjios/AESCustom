//
//  CTPayEncriptyUtil.h
//  CTPay
//
//  Created by zmjios on 2016/11/17.
//  Copyright © 2016年 ctrip. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTPayEncriptyUtil : NSObject


+ (NSString *)encryptRandomCode:(NSString *)randomCode tcTime:(NSString *)tcTime;


+ (NSString *)decryptPayCode:(NSString *)payCode;


+ (NSString *)getTcdtime:(NSString *)payCode;

@end
