//
//  daes.h
//  AESTest
//
//  Created by zmjios on 2016/10/26.
//  Copyright © 2016年 zmjios. All rights reserved.
//

#ifndef daes_h
#define daes_h

#include <stdio.h>
#include <stdint.h>

//秘钥扩展
void key_expansion(uint32_t *key, uint32_t *w);

//加密
void cipher(uint32_t *in, uint32_t *out, uint32_t *w);
//解密
void inv_cipher(uint32_t *in, uint32_t *out, uint32_t *w);


//列混淆
void mix_columns(uint32_t *state);
//反转列混淆
void inv_mix_columns(uint32_t *state);

//行移位
void shift_rows(uint32_t *state);
//反转行移位
void inv_shift_rows(uint32_t *state);

//S盒替换
void sub_bytes(uint32_t *state, uint32_t len);
//反转S盒替换
void inv_sub_bytes(uint32_t *state, uint32_t len);

//轮秘钥控制
void control_round_key(uint32_t *state, uint32_t *k, uint32_t time, uint32_t len);
//反转轮秘钥控制运算
void inv_control_round_key(uint32_t *state, uint32_t *k, uint32_t time, uint32_t len);



#endif /* daes_h */
