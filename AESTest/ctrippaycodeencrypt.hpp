//
//  ctrippaycodeencrypt.hpp
//  AESTest
//
//  Created by zmjios on 2016/11/18.
//  Copyright © 2016年 zmjios. All rights reserved.
//

#ifndef ctrippaycodeencrypt_hpp
#define ctrippaycodeencrypt_hpp

#include <stdint.h>
#include <cstdint>
#include <string>

using namespace std;

class CTPayCodeEncrypt{
    
public:
    CTPayCodeEncrypt();
    static string encrypt(const string &randomcode, const string &tctime);
    static string decrypt(const string &paycode);
    
private:
    static int getMaxLength(unsigned long decimal);
    static void toBinary(unsigned long decimal, char *output);
    static bool isContentsValid(const string &content);
    static string stringToBinaryString(const string &randomCode);
    static string calculateKey(const string &shaTc);
    
};

#endif /* ctrippaycodeencrypt_hpp */
