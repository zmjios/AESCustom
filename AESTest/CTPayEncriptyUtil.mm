//
//  CTPayEncriptyUtil.m
//  CTPay
//
//  Created by zmjios on 2016/11/17.
//  Copyright © 2016年 ctrip. All rights reserved.
//

#import "CTPayEncriptyUtil.h"
#include <stdlib.h>
#include "QRPayEncryption.hpp"

@implementation CTPayEncriptyUtil

+ (NSString *)encryptRandomCode:(NSString *)randomCode tcTime:(NSString *)tcTime
{
    const char *randomChar = [randomCode UTF8String];
    const char *tcTimeChar = [tcTime UTF8String];
    std::string code = QRPayEncryption().encrypt(randomChar, tcTimeChar);
    
    NSString *realCode = @"";
    if (code.c_str() != NULL) {
        realCode = [NSString stringWithUTF8String:code.c_str()];
    }
    
    return realCode;
}

+ (NSString *)decryptPayCode:(NSString *)payCode
{
    const char *payCodeChar = [payCode UTF8String];
    std::string randomCode = QRPayEncryption().decrypt(payCodeChar);
    
    NSString *realCode = @"";
    if (randomCode.c_str() != NULL) {
        realCode = [NSString stringWithUTF8String:randomCode.c_str()];
    }
    
    return realCode;
}


+ (NSString *)getTcdtime:(NSString *)payCode
{
    const char *payCodeChar = [payCode UTF8String];
    std::string tcd = QRPayEncryption().getTcdtime(payCodeChar);
    
    NSString *realCode = @"";
    if (tcd.c_str() != NULL) {
        realCode = [NSString stringWithUTF8String:tcd.c_str()];
    }
    
    return realCode;
}


@end
