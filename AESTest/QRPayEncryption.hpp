//
//  QRPayEncryption.hpp
//  QRPayEncryption
//
//  Created by zmjios on 2016/11/25.
//  Copyright © 2016年 zmjios. All rights reserved.
//

#ifndef QRPayEncryption_hpp
#define QRPayEncryption_hpp

#include <stdio.h>
#include <cstdint>
#include <string>


class QRPayEncryption{
    
public:
    QRPayEncryption();
    static std::string encrypt(const char *randomcode, const char *tctime);
    static std::string decrypt(const char *paycode);
    static std::string getTcdtime(const char *paycode);
    
private:
    static int getMaxLength(unsigned long decimal);
    static void toBinary(unsigned long decimal, char *output);
    static bool isContentsValid(const std::string &content);
    static std::string stringToBinaryString(const std::string &randomCode);
    static std::string calculateKey(const std::string &shaTc);
    static std::string getVector();
    static std::string itos(long i);
    
};


#endif /* QRPayEncryption_hpp */
